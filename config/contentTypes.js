var path = require('path');

module.exports = {
    'blank_wysiwyg': {
        title: 'Blank WYSIWYG',
        model: 'Content',
        template: path.resolve(__dirname, '../apps/demo-page/templates/content/blank.ejs'),
        fields: [
            { name: "body", label: 'HTML', type: 'html' }
        ]
    },
    
    'blank_html': {
        title: 'Blank HTML',
        model: 'Content',
        template: path.resolve(__dirname, '../apps/demo-page/templates/content/blank.ejs'),
        fields: [
            { name: "body", label: 'HTML', type: 'textarea' }
        ]
    },
    
    'article': {
        title: 'Article',
        model: 'Content',
        template: path.resolve(__dirname, '../apps/demo-page/templates/content/article.ejs'),
        fields: [
            { name: "headline", label: 'Headline', type: 'text' },
            { name: "body", label: 'Body', type: 'html' }
        ]
    },
    
    'referral': {
        title: 'Referral',
        model: 'Content',
        template: path.resolve(__dirname, '../lib/referral/template.ejs'),
        render: require('../lib/referral/render.js'),
        
    },

    'forwardto': {
        title: 'Forward to URL',
        model: 'Content',
        template: null,
        render: require('../lib/forwardto/forward'),
        fields: [
            { name: 'url', label: 'URL', type: 'text' },
        ]
    },
 
    'register': {
        title: 'Register User',
        model: 'Content',
        template: path.resolve(__dirname, '../lib/user/register/template.ejs'),
        fields: [{
            name: 'description',
            label: 'Formular Beschreibung',
            type: 'html'
        },{
            name: 'success',
            label: 'Formular Erfolgstext',
            type: 'html'
        }]
    },

    'confirmMail': {
        title: 'E-Mail-Adresse bestätigen',
        model: 'Content',
        template: path.resolve(__dirname, '../lib/user/confirmMail/template.ejs'),
        render: require('../lib/user/confirmMail/render.js'),
        fields: []
    },

    'resetPasswordRequest': {
        title: 'Password zurücksetzen anfordern',
        model: 'Content',
        template: path.resolve(__dirname, '../lib/user/requestPasswordReset/template.ejs'),
        fields: [{
                name: 'description',
                label: 'Formular Beschreibung',
                type: 'html'
            },{
                name: 'success',
                label: 'Formular Erfolgstext',
                type: 'html'
            }
        ]
    },

    'resetPassword': {
        title: 'Password zurücksetzen',
        model: 'Content',
        template: path.resolve(__dirname, '../lib/user/resetPassword/template.ejs'),
        render: require('../lib/user/resetPassword/render'),
        fields: [{
                name: 'description',
                label: 'Formular Beschreibung',
                type: 'html'
            },{
                name: 'success',
                label: 'Formular Erfolgstext',
                type: 'html'
            },{
                name: 'requestPage',
                label: 'Passwort zurücksetzten Anfragen Page',
                type: 'relation',
                options: {
                    model: 'Page',
                    multiple: false
                }
            }
        ]
    },

    'changePassword': {
        title: 'Password ändern',
        model: 'Content',
        template: path.resolve(__dirname, '../lib/user/changePassword/template.ejs'),
        fields: [{
                name: 'description',
                label: 'Formular Beschreibung',
                type: 'html'
            },{
                name: 'success',
                label: 'Formular Erfolgstext',
                type: 'html'
            },{
                name: 'requestPage',
                label: 'Passwort zurücksetzten Anfragen Page',
                type: 'relation',
                options: {
                    model: 'Page',
                    multiple: false
                }
            }
        ]
    },

    'userProfile': {
        title: 'Benutzerprofil',
        model: 'Content',
        template: path.resolve(__dirname, '../lib/user/profile/template.ejs'),
        render: require('../lib/user/profile/render'),
        fields: [{
                name: 'description',
                label: 'Formular Beschreibung',
                type: 'html'
            },{
                name: 'success',
                label: 'Formular Erfolgstext',
                type: 'html'
            },{
                name: 'changePasswordPage',
                label: 'Passwort ändern Seite',
                type: 'relation',
                options: {
                    model: 'Page',
                    multiple: false
                }
            }
        ]
    },
    
    'newsletterRegistration': {
        title: 'Newsletter Anmeldung',
        model: 'Content',
        template: path.resolve(__dirname, '../lib/newsletter/register/template.ejs'),
        fields: [{
            name: 'success',
            label: 'Formular Erfolgstext',
            type: 'text'
        }]
    }
};

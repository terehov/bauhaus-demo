/// <reference path="../typings/node/node.d.ts"/>
var contentTypes = require('./contentTypes'),
    pageTypes = require('./pageTypes'),
    securityConfig = require('bauhausjs/security/config.js'),
    userConfig = require('../lib/user/config.js'),
    emailConfig = require('../lib/email/config.js'),
    pageConfig = require('bauhausjs/page/config.js');

// Build configuration
var bauhausConfig = module.exports = {
    port: Number(process.env.PORT                                   || 1919),
    env: process.env.NODE_ENV                                       || 'development',

    mongodb: process.env.MONGOSOUP_URL                              || 'mongodb://localhost/bauhausdemo',
    pkgcloud: {
        provider:           'openstack',
        username:           process.env.OS_USER      || 'username',
        password:           process.env.OS_PWD       || 'password',
        authUrl:            process.env.OS_AUTHURL   || 'https://identity-server...',
        basePath:           'v2.0/tokens',
        useServiceCatalog:  'true',
        region:             'RegionOne'
    },
    swiftPublicFilesURL: 'https://object-store-server...',
    
    contentTypes: contentTypes,
    pageTypes: pageTypes,

    documents: {},
    addDocument: function (name, config) {
        this.documents[name] = config;
    },

    addCustomUserField: function (fields) {
        fields = Array.isArray(fields) ? fields : [fields];
        for (var f in fields) {
            this.customUserFields.push(fields[f]);
        }
    },

    security: {
        sessionSecret: '123456',
        path: '/',
        maxAge: 100 * 60 * 60 * 24,

        permissions: {},
        addPermission: function (pluginName, permissions) {
            this.permissions[pluginName] = permissions;
        },
    },

    email: {
        module: process.env.MAIL_PROTOCOL   ||  'direct',
        config: {
            debug: process.env.MAIL_DEBUG   ||  true,
            host: process.env.MAIL_HOST     ||  '',
            port: process.env.MAIL_PORT     ||  25,
            secure: process.env.MAIL_SECURE ||  false,
            auth: {
                user: process.env.MAIL_USER ||  '',
                pass: process.env.MAIL_PWD  ||  ''
            }
        },
        concurrency: 100,
        mailSender: 'no-reply@bauhaus.io',
    },

    mailchimp: {
        apikey: process.env.MAILCHIMP_APIKEY || 'API_KEY_DEFAULT',
        lists: {
            'default': process.env.MAILCHIMP_DEFAULTLIST || 'DEFAULT_LIST'
        }
    },
    
    user: {
        mailConfirmMail:            "Register:ConfirmMail",
        mailResetPassword:          "User:resetPassword"
    },

};

securityConfig(bauhausConfig); // registers user and role admin

// Config custom modules
userConfig(bauhausConfig);
emailConfig(bauhausConfig);
pageConfig(bauhausConfig);

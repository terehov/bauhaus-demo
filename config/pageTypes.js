var path = require('path');

path.resolve(__dirname, '../apps/demo-page/templates/content.ejs'),

module.exports = {

    'layout': {
        title: 'Home page tempate',
        model: 'Page',
        template: path.resolve(__dirname, '../apps/demo-page/templates/layout.ejs'),
        slots: [
            {name:'content', label: 'Content'}
        ]
    }
};
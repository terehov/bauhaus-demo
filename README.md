# BauhausJS-Demo

bauhausJS demo project

## Installation

BauhausJS requires a running MongoDB. 

    $ git clone https://github.com/...
    $ cd bauhausjs-demo/
    $ npm install
    $ node index.js

Visit http://localhost:1919/ to see the website, and http://localhost:1919/backend/ to login into backend (default user after installation: test@bettervest.de / test).
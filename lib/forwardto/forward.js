module.exports = function (req, res, data, template, callback) {
    if (data.content && data.content.url) { 
        res.redirect(data.content.url);
    } else{
        res.status(404).send('Not found');
    }
};
var Intl            = require("intl");

// swig extensions
module.exports = function(pSwig){

    
    // → 123.456,79 €
    pSwig.setFilter('numAsEuro', function (input, idx) {
      return numAsEuro(input);
    });
    
    // → 123.456,79 
    pSwig.setFilter('numAsLocNumber', function (input, idx) {
      return numAsLocNumber(input);
    });
    
    /**
    * DATE FUNCTIONS
    */
    
    // Add days to date
    pSwig.setFilter('datePlusDays', function (input, idx) {
        return new Date(input).setDate(input.getDate() + idx);
    });
    // Subtract days to date
    pSwig.setFilter('dateMinusDays', function (input, idx) {
        return new Date(input).setDate(input.getDate() - idx);
    });
    
    // Add months to date
    pSwig.setFilter('datePlusMonths', function (input, idx) {
        return new Date(input).setMonth(input.getMonth() + idx);
    });
    // Subtract months to date
    pSwig.setFilter('dateMinusMonths', function (input, idx) {
        return new Date(input).setMonth(input.getMonth() - idx);
    });
    
    // Add years to date
    pSwig.setFilter('datePlusYears', function (input, idx) {
        var newYear = 1900 + (input.getYear() + idx);
        return new Date(input).setYear(newYear);
    });
    // Subtract years to date
    pSwig.setFilter('dateMinusYears', function (input, idx) {
        var newYear = 1900 + (input.getYear() - idx);
        return new Date(input).setYear(newYear);
    });
    
    /** 
     * Helper functions
     */
     
    function numAsEuro(pNum){
        if(isNaN(pNum))
            pNum = 0;
        
        return new Intl.NumberFormat("de-DE", {style: "currency", currency: "EUR"}).format(pNum);
    }
    
    function numAsLocNumber(pNum){
        if(isNaN(pNum))
            pNum = 0;
        
        return new Intl.NumberFormat("de-DE", {style: "decimal", currency: "EUR"}).format(pNum);
    }
    return pSwig;

}
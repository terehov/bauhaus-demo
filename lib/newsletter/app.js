var express = require('express');
var subscribe = require('./subscribe');
var debug = require('debug')('module:newsletter');

module.exports = function (bauhausConfig) {
    var app = express();

    app.post('/subscribe', function (req, res, next) {
        if (!req.body || (typeof req.body.email !== 'string' && req.body.email.length < 1) ) {
            res.status(400);
            return res.end('No email passed')
        }
        var email = req.body.email;

        var mc = bauhausConfig.mailchimp;
        subscribe(mc.apikey, mc.lists.default, email, function (err, data) {
            if (err) {
                debug('Failed to register to newsletter', err);
                res.status('500');
                return res.json({success:false, error: err.error})
            }

            res.status('200');
            return res.json({success:true});

        });
    });

    return app;
};
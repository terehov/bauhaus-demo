var mailchimpApi = require('mailchimp-api/mailchimp');

module.exports = function subscribe (key, listId, email, callback) {
    var mc = new mailchimpApi.Mailchimp(key);

    mc.lists.subscribe({
        id: listId,
        email: {
            email: email
        }
    }, function (data) {
        callback(null, data);
    }, function (err) {
        callback(err);
    });
}
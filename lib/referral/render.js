//var $                   = require('./settings');
//var investHelper        = require("./helper");
//var bh = require('../../config');

// this is a dynamic content block with full access to original req / res, which allows to intercept the request and return data directly
// dynamic content which is supposed to be injected into the website by calling callback(err, html)


module.exports = function (req, res, data, template, callback) {

    var templateData = {};
    templateData.url = req.protocol + "://"+req.headers.host+"/registrierung";
    templateData.user = {};
    templateData.user           = (req.user != null && req.user.fields != null)? req.user.fields : {};
    templateData.user.id        = (req.user != null && req.user._id != null)? req.user._id : "";
    templateData.user.username  = (req.user != null && req.user.username != null)? req.user.username : "";
    
    res.render(template, templateData, callback);

}
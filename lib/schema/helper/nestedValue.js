
var nestedValue = module.exports = {};


/**
 * Returns value of key in nested object
 * 
 * @param  {Object} obj Object value is read from
 * @param  {String} key Key which should be read, e.g. 'address.street'
 * @return {any}    Returns value at key, undefined if does not exists
 */

nestedValue.get = function getNestedValue(obj, key) {
    try {
        // from http://stackoverflow.com/a/10934946
        return key.split(".").reduce(function(o, x) { return o[x] }, obj);
    } catch (error) {
        // requested unexisting property
        return undefined;
    }
};

/**
 * Sets value in nested object
 * 
 * @param {Object} obj   Object you want to write to (is manipulated directly, no need to assign result)
 * @param {[type]} key   Key, you want to write to, e.g. 'adress.street'
 * @param {[type]} value Value you want to set at key
 */
nestedValue.set = function setNestedValue(obj, key, value) {
    var keys = key.split('.');

    var currentScope = obj;

    for (var i = 0; i < keys.length; i++) {
        if (!currentScope.hasOwnProperty(keys[i])) {
            // create unexistend property
            currentScope[ keys[i] ] = {};
        }
        // set value on last iteration
        if (i + 1 === keys.length) {
            currentScope[ keys[i] ] = value;
        } 
        currentScope = currentScope[ keys[i] ];

    }
    return obj;
}


var getNestedValue = require('../helper/nestedValue').get;
var setNestedValue = require('../helper/nestedValue').set;

/**
 * Consumes an object and returns only the fields which given role has permission for of the original object
 * 
 * @param  {Object} data        Object which should be filtered
 * @param  {Object} schema      Schema containing the filter information, e.g. lib/project/config.js
 * @param  {String} role        Role of user you want to filter
 * @param  {Array}  permissions Array of permissions which need to be set
 * @return {Object}             Filtered input data result
 */
module.exports = function filterBySchema (data, schema, role, permissions) {
    permissions = Array.isArray(permissions) ? permissions : [permissions];

    var result = {};
    for (var f in schema.fields) {
        var field = schema.fields[f];

        for (var p in permissions) {
            var permission = permissions[p];
            if (field.permission && field.name) {
                // check if permission exists
                if (typeof field.permission[ permission ] === 'object') {
                    // check if role has permission
                    if (typeof field.permission[ permission ][ role ] === 'boolean' && field.permission[ permission ][ role ] === true) {
                        // read value from input object (field can be nested)
                        var value = getNestedValue(data, field.name);
                        if (typeof value !== 'undefined') {
                            // set value to result
                            setNestedValue(result, field.name, value);
                        }
                    }
                }

            }
        }
    }

    return result;
}
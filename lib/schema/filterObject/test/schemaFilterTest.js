
var assert = require('assert');
var filterSchema = require('../index');

describe('filterSchema', function () {
    it('should only return fields role has permission for', function () {
        var obj = {
            'field1': true,
            'field2': true,
            'field3': true
        };

        var schema = {
            fields: [
                {
                    name: 'field1',
                    permission: {
                        edit: {
                            owner: true
                        }
                    }
                },
                {
                    name: 'field2',
                    permission: {
                        edit: {
                            owner: true
                        }
                    }
                }, 
                {
                    name: 'field3'
                }
            ]
        }

        var role = 'owner';
        var permission = 'edit';
        var result = filterSchema(obj, schema, role, permission);

        assert(obj.field1 === result.field1, 'Result should contain field1');
        assert(obj.field2 === result.field2, 'Result should contain field2');
        assert(typeof result.field3 === 'undefined', 'Result should lack field3');
    });

    it('should work with nested values', function () {
        var obj = {
            'field1': {
                'sub': {
                    'level': true
                }
            }
        };

        var schema = {
            fields: [
                {
                    name: 'field1.sub.level',
                    permission: {
                        edit: {
                            owner: true
                        }
                    }
                }
            ]
        }

        var role = 'owner';
        var permission = 'edit';
        var result = filterSchema(obj, schema, role, permission);

        assert(obj.field1.sub.level === result.field1.sub.level, 'Result should contain nested field');
    });

    it('should work with multiple permissions', function () {
        var obj = {
            'field1': true,
            'field2': true
        };

        var schema = {
            fields: [
                {
                    name: 'field1',
                    permission: {
                        edit: {
                            owner: true
                        }
                    }
                }, {
                    name: 'field2',
                    permission: {
                        view: {
                            owner: true
                        }
                    }
                }
            ]
        }

        var role = 'owner';
        var permission = ['edit', 'view'];
        var result = filterSchema(obj, schema, role, permission);

        assert(obj.field1 === result.field1, 'Result should contain field1');
        assert(obj.field2 === result.field2, 'Result should contain field2');
    });
});
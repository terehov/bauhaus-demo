/**
 * Filter passed schema that it only contains fields role has permission for and return updated schema
 */

module.exports = function filterSchema (schema, role, permissions) {
    permissions = Array.isArray(permissions) ? permissions : [permissions];
    var schemaCopy = JSON.parse(JSON.stringify(schema));

    var result = {
        fields: []
    };

    var fieldsetIds = {};

    for (var f in schemaCopy.fields) {
        var field = schemaCopy.fields[f];

        for (var p in permissions) {
            var permission = permissions[p];
            if (field.permission && field.name) {
                // check if permission exists
                if (typeof field.permission[ permission ] === 'object') {
                    // check if role has permission for field
                    if (typeof field.permission[ permission ][ role ] === 'boolean' && field.permission[ permission ][ role ] === true) {
                        // copy field to output schema
                        // remove original settings to hide info about other roles
                        
                        field.permission = permission;
                        result.fields.push(field);

                        // add fieldset to list, if existend
                        if (field.fieldset) {
                            fieldsetIds[ field.fieldset ] = true;
                        }
                    }
                }
            }
        }
    }

    // Copy all used fieldsets to sub schema
    if (Array.isArray(schemaCopy.fieldsets) && schemaCopy.fieldsets.length > 0) {
        result.fieldsets = [];
        for (var s in schemaCopy.fieldsets) {
            var currentFieldset = schemaCopy.fieldsets[ s ];
            if ( fieldsetIds.hasOwnProperty( currentFieldset.id ) === true) {
                result.fieldsets.push( currentFieldset );
            }
        }
    }

    var fieldsets = schemaCopy.fieldsets;

    return result;
}
var getNestedValue = require('../helper/nestedValue').get;

/**
 * Validates user object against a given schema
 *
 * Check out lib/user/schema for schema examples
 *
 * @example
 *     var validateSchema = require('./schema/validate');
 *     var result = validateSchema( user, schema );
 */

module.exports = function validateSchema (data, schema) {
    var result = {
        valid: true,
        errors: {}
    }

    function addError (field, type) {
        if (typeof result.errors[ field ] === 'undefined') {
            result.errors[ field ] = {};
        }
        result.valid = false;
        result.errors[ field ][ type ] = false;
    }

    
    for (var f in schema.fields) {
        var field = schema.fields[f];
        var value = getNestedValue(data, field.name);

        if (field.options) {
            if (field.options.required === true) {
                // check if required field exists
                if (value === null || value === undefined) {
                    // required field is null or undefined, race error
                    addError(field.name, 'required');
                }
            }

            if (typeof field.options.minlength === 'number') {
                if (value.length < field.options.minlength) {
                    addError(field.name, 'minlength')
                }
            }

            if (typeof field.options.maxlength === 'number') {
                if (value.length > field.options.maxlength) {
                    addError(field.name, 'maxlength')
                }
            }

        }
    }
    return result;
}
var assert = require('assert');

var validateSchema = require('../index.js');

describe('validateSchema', function () {

    it('should return no error on option require when value exists', function () {

        var obj = {
            'username': '123'
        };
        var schema = {
            fields: [
                {
                    name: 'username',
                    type: 'text',
                    options: {
                        required: true
                    }
                }
            ]
        }

        var result = validateSchema(obj, schema);
        assert(result.valid === true, 'Expect object to be valid');
        assert(typeof result.errors.username === 'undefined', 'Expect no missing username validation error');
    });

    it('should return no error on option require when value exists for nested values', function () {

        var obj = {
            'user': {
                'name': '123'
            }
        };

        var schema = {
            fields: [
                {
                    name: 'user.name',
                    type: 'text',
                    options: {
                        required: true
                    }
                }
            ]
        }

        var result = validateSchema(obj, schema);
        assert(result.valid === true, 'Expect object to be valid');
        assert(typeof result.errors['user.name'] === 'undefined', 'Expect no missing username validation error');
    });

    it('should return error option require when value is missing', function () {

        var obj = {};
        var schema = {
            fields: [
                {
                    name: 'username',
                    type: 'text',
                    options: {
                        required: true
                    }
                }
            ]
        }

        var result = validateSchema(obj, schema);
        assert(result.valid === false, 'Expect object to be invalid');
        assert(result.errors.username.required === false, 'Expect missing username validation error');
    });

    it('should return error on option require when nested value is missing', function () {

        var obj = {
            'user': {}
        };
        var schema = {
            fields: [
                {
                    name: 'user.name',
                    type: 'text',
                    options: {
                        required: true
                    }
                }
            ]
        }

        var result = validateSchema(obj, schema);
        assert(result.valid === false, 'Expect object to be invalid');
        assert(result.errors['user.name'].required === false, 'Expect missing username validation error');
    });

    it('should return error on option minlength when value to short', function () {

        var obj = {
            'username': '1234'
        };
        var schema = {
            fields: [
                {
                    name: 'username',
                    type: 'text',
                    options: {
                        minlength: 5
                    }
                }
            ]
        }

        var result = validateSchema(obj, schema);
        assert(result.valid === false, 'Expect object to be invalid');
        assert(result.errors.username.minlength === false, 'Expect to short username validation error');
    });

    it('should return no error on option minlength when value long enough', function () {

        var obj = {
            'username': '12345'
        };
        var schema = {
            fields: [
                {
                    name: 'username',
                    type: 'text',
                    options: {
                        minlength: 5
                    }
                }
            ]
        }

        var result = validateSchema(obj, schema);
        assert(result.valid === true, 'Expect object to be valid');
        assert(typeof result.errors.username === 'undefined', 'Expect to long enough username validation error');
    });   

    it('should return error on option maxlength when value to long', function () {

        var obj = {
            'username': '12345'
        };
        var schema = {
            fields: [
                {
                    name: 'username',
                    type: 'text',
                    options: {
                        maxlength: 4
                    }
                }
            ]
        }

        var result = validateSchema(obj, schema);
        assert(result.valid === false, 'Expect object to be invalid');
        assert(result.errors.username.maxlength === false, 'Expect to long username validation error');
    });

    it('should return no error on option maxlength when value short enough', function () {

        var obj = {
            'username': '12345'
        };
        var schema = {
            fields: [
                {
                    name: 'username',
                    type: 'text',
                    options: {
                        maxlength: 5
                    }
                }
            ]
        }

        var result = validateSchema(obj, schema);
        assert(result.valid === true, 'Expect object to be valid');
        assert(typeof result.errors.username === 'undefined', 'Expect to long enough username validation error');
    });      
});
var q           = require("q");
var nodemailer  = require('nodemailer');
var EmailModel  = require('./model/email');
var swig        = require("swig");
require("../swighelper")(swig);
var debug       = require("debug");

module.exports = function (bauhausConfig) {
    
    var emailHelper = {};
    if(bauhausConfig.email.transport == null){
        switch (bauhausConfig.email.module) {
          case "smtp":
            var smtpTransport = require('nodemailer-smtp-transport');
            bauhausConfig.email.transport  = nodemailer.createTransport(smtpTransport (bauhausConfig.email.config));
            break;
          default:
            var directTransport = require('nodemailer-direct-transport');
            bauhausConfig.email.transport  = nodemailer.createTransport(directTransport(bauhausConfig.email.config));
        }
    }
    
    
    emailHelper.sendMail = function(pMail){
        var deferred = q.defer();
        bauhausConfig.email.transport.sendMail(pMail, function (err) {
            if (err){
                debug("email",'Was not able to send mail:', pMail);
                deferred.reject(err);
            } else {
                deferred.resolve(true);
            }
        });
        
        return deferred.promise;
    };
    
    
    emailHelper.getMailTemplates = function(){
        var deferred = q.defer();
        EmailModel.find(function(err, email){
            if (err){
                deferred.reject(err);
            } else {
                deferred.resolve(email);
            }
        });
        
        return deferred.promise;
    };
    
    emailHelper.replaceAmpChar = function(pData){
        if(typeof pData === "string")
            pData = pData.replace("&amp;", "&");
        
        return pData;
    }
    
    // will user "bauhausConfig.email.mailSender" if from wasnt defined
    emailHelper.sendTemplateMail = function(pName, pTo, pData, pAttachments){
        var deferred = q.defer();
        
        var tmplVars = {
            locals: pData
        };
        
        _getMailByName(pName).then(function(pEmail){
            
            // compose and render mail
            var emailObject = {
                from:           pEmail.from || bauhausConfig.email.mailSender || "localhost",
                to:             pTo,
                subject:        emailHelper.replaceAmpChar(swig.render(pEmail.subject, tmplVars)), // render subject
                text:           emailHelper.replaceAmpChar(swig.render(pEmail.content, tmplVars)),  // render content
                attachments:    pAttachments // add attachments if avaliable
            };

            // send mail        
            emailHelper.sendMail(emailObject).then(function(){
                deferred.resolve(true);
            }, function(err){
                debug("email",'emailHelper.sendTemplateMail.sendMail', err);
                deferred.reject(err);
            });
            
        }, function(err){
            debug("email",'emailHelper.sendTemplateMail', err);
            deferred.reject(err);
        });
        return deferred.promise;
    };
    
    function _getMailByName(pName){
        var deferred = q.defer();
        EmailModel.findOne({title: pName}, function(err, email){
            if (err){
                deferred.reject(err);
            } else {
                deferred.resolve(email);
            }
        });
        
        return deferred.promise;
    };
    
    return emailHelper;    
    
};
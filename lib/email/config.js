
module.exports = function (bauhausConfig) {
    bauhausConfig.addDocument('Emails', {
        name: 'Email',
        model: 'Email',
        collection: 'emails',
        icon: 'envelope',
        fields: [
            { 
                name: 'title',
                type: 'text',
                label: 'Title'
            }, 
            { 
                name: 'subject',
                type: 'text',
                label: 'Betreff'
            }, 
            { 
                name: 'content',
                type: 'textarea',
                label: 'Inhalt'
            }
        ]
    });
}

var baucis  = require('baucis'),
    Email   = require('./model/email'),
    mongoose = require('mongoose');

module.exports = function (bauhausConfig) {

    var emailController = baucis.rest(mongoose.model('Email')).select('title content subject');

    return emailController;
};

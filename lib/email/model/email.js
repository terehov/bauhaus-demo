var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var emailSchema = new Schema({
    title:      String,
    subject:    String,
    content:    String
});

var email = module.exports = mongoose.model('Email', emailSchema);
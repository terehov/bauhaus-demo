# User

This submodule wraps all project specific user functionality. The login endpoint is provided by Bauhaus, all other user functionality is provided by this module.


While frontend user schemata are defined in `/schema/`, their exists only one schema in backend, which is described in `config.js`.

## Register

Registers user and sends mail with e-mail confirmation code. Registration makes no use of frontend user schemata, it only requests an additional field for companys name if a non-person type is selected.

### Content Widget

#### `register`

Should be added under route `/registrieren` (linked in user bar)

Fields

 * Text `description` (shown above form)
 * Text `success` (shown after successful registration)

### REST Endpoint
 
* `POST /register`



## Confirm mail

Is called with token by user (link from registration mail) to confirm validty of the e-mail address.

### Content Widget

### `confirmMail`

Must be added with path `/confirmMail` (Link in Register mail).

This is a dynamic widget which processes user request directly, therefore no REST Endpoint is required.

Fields: none



## Reset Password Request

Shows form to enter e-mail and sends a mail with link and token to password reset page.

### Content Widget

#### `resetPasswordRequest`

Can be added to any route (only dynamic reference in `resetPassword` content widget).

Fields

 * Text `description` (shown above form)
 * Text `success` (shown after successful registration)

### Rest Endpoint

* `POST /requestPasswordReset`



## Reset Password

Shows a form to set new password if token is valid.

### Content Widget

#### `resetPassword`

Needs to be added to route `/reset`. (Reset e-mail contains link).

Fields

 * Text `description` (shown above form)
 * Text `success` (shown after successful registration)
 * Page `requestPage` (Link to request password page is shown if user enters invalid token)

### REST Endpoint

* `POST /resetPassword/:token`



## Change Password

Shows a form to set new password for currently authorized user.

### Content Widget

#### `changePassword`

Can be added to any route. Page should be set to secure, which allows only authorized users to change their passwords.

Fields

 * Text `description` (shown above form)
 * Text `success` (shown after successful registration)

### REST Endpoint

* `POST /changePassword`



## Profile

Shows and validates user profile. Depending on type of user model (value of `fields.type`, e.g. `person`, `gmbh`).

### Content Widget

#### `userProfile`

Should be added to page with path `/profil`, because this page is hard-linked at several places (disruptor, user bar)

Fields: none

### REST Endpoint

* `GET /user/profile` (returns current user profile with fields `username` and `fields`, the object containing all custom user data)
* `PUT /user/profile` (updates profile if passed user is valid for user schema)
* `GET /user/schema` (returns and object of all defined user schemata)



// Security frontend REST API endpoints for password reset

var express = require('express');
var nodemailer = require('nodemailer');
var sha1 = require('sha1');
var User = require('bauhausjs/security/model/user');

module.exports = function (bauhausConfig) {
    var app = express();

    app.post('/changePassword', function (req, res, next) {
        if (!req.session || !req.session.user) {
            res.status(403);
            return res.send('Forbidden');
        }

        var oldPassword = req.body.oldPassword;
        var password1 = req.body.password1;
        var password2 = req.body.password2;

        if (typeof password1 === 'string' && password1.length < 6) {
            res.status(400);
            return res.end('New password must be at least 6 characters long')
        }

        if (password1 !== password2) {
            res.status(400);
            return res.end('New passwords do not match');
        }

        User.findOne({ username: req.session.user.username }, function (err, user) {
            if (err || user === null) {
                res.status(404);
                return res.end('Not found');
            }

            user.authenticate(oldPassword, function (err, authenticated, message) {
                if (err || authenticated === false) {
                    res.status(403);
                    return res.end('Forbidden');
                }

                // user for token was found, reset password
                user.setPassword(password1, function (err) {
                    if (err) {
                        res.status(400);
                        return res.end('Could not reset password');
                    }

                    // save user
                    user.save(function (err) {
                        if (err) {
                            res.status(400);
                            return res.end('Could not reset password');
                        }
                        // successfully saved password
                        return res.json({success:true});
                    });
                });

            });

            
        })
    });

    return app;
}
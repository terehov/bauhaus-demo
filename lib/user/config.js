
module.exports = function (bauhausConfig) {
    bauhausConfig.addDocument('Users', {
        name: 'User',
        model: 'User',
        collection: 'bauhaus-users',
        icon: 'user',
        query: {},
        useAsLabel: 'username',
        fieldsets: [
            { id: 'user',   legend: 'User' },
            { id: 'person', legend: 'Person' },
        ],
        fields: [
            { 
                name: 'username',
                type: 'text',
                label: 'Benutzername (E-Mail-Adresse)',
                fieldset: 'user'
            }, {
                name: 'password',
                type: 'password',
                label: 'Set new password',
                fieldset: 'user'
            }, {
                name: 'roles',
                type: 'roles',
                label: 'Roles',
                fieldset: 'user'
            }, {
                name: 'fields.person.gender',
                type: 'enum',
                label: 'Anrede',
                fieldset: 'person',
                options: {
                    enums: {
                        female: 'Frau',
                        male: 'Herr'
                    }
                }
            }, { 
                name: 'fields.person.firstname',
                type: 'text',
                label: 'Vorname',
                fieldset: 'person'
            },  { 
                name: 'fields.person.lastname',
                type: 'text',
                label: 'Nachname',
                fieldset: 'person'
            }, { 
                name: 'fields.person.birthday',
                type: 'text',
                label: 'Geburtstag',
                fieldset: 'person'
            },{ 
                name: 'fields.person.address.street',
                type: 'text',
                label: 'Straße',
                fieldset: 'person'
            }, { 
                name: 'fields.person.address.streetNo',
                type: 'text',
                label: 'Hausnummer',
                fieldset: 'person'
            },{ 
                name: 'fields.person.address.postcode',
                type: 'text',
                label: 'Postleitzahl',
                fieldset: 'person'
            }, { 
                name: 'fields.person.address.place',
                type: 'text',
                label: 'Ort',
                fieldset: 'person' 
            },  { 
                name: 'fields.person.phone',
                type: 'text',
                label: 'Telefon',
                fieldset: 'person' 
            },
            {
                name: 'fields.person.image',
                type: 'file',
                label: 'Profil-Foto',
                fieldset: 'person',
                _idName: 'userId',
                configPath: 'documents.Users',
                options: {
                    limit: 1,
                    filename: 'profile',
                    singlefile: true,
                    typeRegEx: 'image\/[.]*',
                    cropping: {
                        width: 150,
                        height: 150,
                        maxSize: false,
                        circle: true
                    }
                }
            }
        ]
    });
}

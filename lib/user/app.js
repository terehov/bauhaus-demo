/** 
 * This server combines various REST APIs for users which are added to frontend app
 */

var express = require('express');

var resetPasswordApp = require('./resetPassword/app');
var changePasswordApp = require('./changePassword/app');
var profileApp = require('./profile/app');
var registerApp = require('./register/app');
var validateUserMiddleware = require('./validateUser/middleware');

module.exports = function (bauhausConfig) {
    
    var app = express();

    app.use( changePasswordApp(bauhausConfig) );
    app.use( resetPasswordApp(bauhausConfig) );
    app.use( profileApp(bauhausConfig) );
    app.use( registerApp(bauhausConfig) );

    app.use( validateUserMiddleware(bauhausConfig) );

    app.get('/user/info', function (req, res, next) {
        if (req.session.user) {
            res.send({ username: req.session.user.username })
        } else {
            res.send({ username: undefined });
        }
    });
    
    return app; 
}

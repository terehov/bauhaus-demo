
/**
 * Provides a middleware which adds user validation to user once per session
 */

var User = require('bauhausjs/security/model/user');
var validateSchema = require('../../schema/validate');
var schemata = require('../schema/');

module.exports = function (bauhausConfig) {

    return function validateUser (req, res, next) {

        if (req.user) {
            // method which evaluates all single validations in req.session.user.valid and 
            // returns true if all are valid
            req.user.isValid = function () {
                for (var validation in req.session.user.valid) {
                    if (req.session.user.valid[ validation ] === false) {
                        return false;
                    }
                }
                return true;
            }   
        }

        if (req.session && req.session.user && req.session.user.fields && typeof req.session.user.valid === 'undefined') {
            var user = req.session.user;
            var validation = {};

            if (user.fields && user.fields.type && schemata[ user.fields.type ]) {
                var userSchema = schemata[ user.fields.type ];
                var result = validateSchema( user, userSchema );
                validation.profile = result.valid;
            } else {
                validation.profile = false;
            }
            // persist to session
            req.session.user.valid = validation;
            
            next();

        } else {
            next();
        }
    };

};
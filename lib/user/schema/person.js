module.exports = {
    type: 'person',
    fieldsets: [
        { id: 'person', legend: 'Person' }
    ],
    fields: [
        {
            name: 'fields.person.gender',
            type: 'enum',
            label: 'Anrede',
            fieldset: 'person',
            options: {
                enums: {
                    female: 'Frau',
                    male: 'Herr'
                }
            }
        },
        {
            name: 'fields.person.firstname',
            type: 'text',
            label: 'Vorname',
            fieldset: 'person',
            options: {
                required: true
            }
        },{
            name: 'fields.person.lastname',
            type: 'text',
            label: 'Nachname',
            fieldset: 'person',
            options: {
                required: true
            }
        },{
            name: 'fields.person.birthday',
            type: 'date',
            label: 'Geburtsdatum',
            fieldset: 'person',
            options: {
                required: true
            }
        },{
            name: 'fields.person.address',
            type: 'address',
            label: 'Adresse',
            fieldset: 'person',
            options: {
                required: true
            }
        },{
            name: 'fields.person.phone',
            type: 'text',
            label: 'Telefon',
            fieldset: 'person'
        },
        {
            name: 'fields.person.image',
            type: 'file',
            label: 'Profil-Foto',
            fieldset: 'person',
            _idName: 'userId',
            configPath: 'documents.Users',
            options: {
                limit: 1,
                filename: 'profile',
                singlefile: true,
                typeRegEx: 'image\/[.]*',
                cropping: {
                    width: 150,
                    height: 150,
                    maxSize: false,
                    circle: true
                }
            }
        }
    ]
};

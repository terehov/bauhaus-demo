var express = require('express');
var validator = require('validator');
var confirmMail = require('../confirmMail/sendConfirmMail');
var debug = require('debug')('module:user:register');
var User = require('bauhausjs/security/model/user');
var subscribe = require('../../newsletter/subscribe');

User.schema.path('username').validate(function (username) {
   return validator.isEmail(username);
}, 'Please provide a valid mail address');

module.exports = function (bauhausConfig) {
    var app = express();

    var mailer = confirmMail(bauhausConfig);

    app.post('/registerUser', function (req, res, next) {
        var user = req.body;

        var err = {
            errors: {}
        };
        
        if (user.password.length < 6) {
            err.errors.password = {
                message: 'Please enter a password with at least 6 characters.'
            };
        }
        if (typeof user.firstname !== 'string' || user.firstname.length < 2) {
            err.errors.firstname = {
                message: 'Please enter your first name'
            };
        } 
        if (typeof user.lastname !== 'string' || user.lastname.length < 2) {
            err.errors.lastname = {
                message: 'Please enter your last name'
            };
        }
        var validTypes = ['person', 'gmbh'];
        if (validTypes.indexOf( user.type ) === -1) {
            err.errors.type = {
                message: 'Invalid account type'
            };
        }

        if (Object.keys(err.errors).length === 0) {
             
            // if referral --> user avaliable?
            if(user.referral != null){
                User.findOne({
                    _id: user.referral
                }, 'username', function (err, referral) {
                    if (err) {
                        debug("register", 'Cannot find referral user: ', err);
                    }
                    //found user? otherwise without user
                    (referral != null)? _createNewUser(referral._id) : _createNewUser();
                });
            } else{
                _createNewUser();
            }
            
            function _createNewUser(pReferral){
                
                var newUser = new User();
                newUser.username = user.username;
                newUser.fields = {
                    person: {
                        firstname: user.firstname,
                        lastname: user.lastname,
                        gender: user.gender
                    },
                    type: user.type
                };
                if (user.type !== 'person') {
                    newUser.fields.company = {
                        name: user.company
                    }
                }
                // save referral
                if(pReferral != null){
                    newUser.fields.referral = {};
                    newUser.fields.referral.user = pReferral;
                }
                                
                // set an initial token to confirm mail
                newUser.setConfirmMailToken();
                User.register(newUser, req.body.password, function (err) {
                    if (err) {
                        var result;
                        if (!err.errors) {
                            // add error to existing error object if receiving single error
                            // User.register() retruns sometime an validation error object and sometimes
                            // custom errors
                            result = {
                                errors: {
                                    username: err
                                }
                            };
                        } else {
                            // set returned error object to output.error
                            result = err;
                        }
                        // rendert prefilled form with mongoose validation errors
                        res.status(400);
                        return res.json({error: result});
                    } else {
                        // subscribe newsletter
                        if (user.newsletter == true) {
                            var mc = bauhausConfig.mailchimp;
                            // newsletter subscription
                            subscribe(mc.apikey, mc.lists.default, user.username, function (err, data) {
                                if (err) {
                                    debug('Was not able to subscribe user at mailchimp.', err);
                                }
                            });
                        }
                        
                        var host = req.headers.host;
                        // registration successfully, send mail and send success response
                        mailer.send(newUser, host, function (err, mailerResponse) {
                            if (err) {
                                debug('Was not able to send mail.', err);
                            }else {
                                // send success 
                                res.status(200);
                                return res.json({ success:true });
                            }   
                        });
                    }
                });
            
            }
            
        } else {
            res.status(400);
            return res.json({ error: err })
        }
    })

    return app;
}


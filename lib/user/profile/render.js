
module.exports = function (req, res, data, template, callback) {
    var viewData = data.toObject();

    res.render(template, data.content, callback);
};
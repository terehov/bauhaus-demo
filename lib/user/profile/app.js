// This routes are exposed on the frontend app

var express = require('express');
var userSchema = require('../schema/');
var User = require('bauhausjs/security/model/user');
var validateSchema = require('../../schema/validate');

var isAuthenticated = require('bauhausjs/security/middleware').isAuthenticated();

module.exports = function (bauhausConfig) {
    var app = express();

    app.get('/user/profile', isAuthenticated, function (req, res, next) {
        User.findOne({_id: req.session.user.id}, 'username fields', function (err, user) {
            if (err && user !== null) {
                res.status(500);
                return res.end('An error occured');
            } 
            res.json(user);
        })
    });

    app.put('/user/profile', isAuthenticated, function (req, res, next) {
        User.findOne({_id: req.session.user.id}, 'username fields', function (err, user) {
            if (err && user !== null) {
                res.status(500);
                return res.end('An error occured');
            } 
            // validate if type is unchanged and exists
            if (req.body.fields.type !== user.fields.type || userSchema.hasOwnProperty(user.fields.type) === false) {
                res.status(400);
                return res.send("Invalid type")
            } else {
                var schema = userSchema[ user.fields.type ]
                // validate userdata
                var validation = validateSchema(req.body, schema);
                if (validation.valid !== true) {
                    res.status(400);
                    return res.json(validation);
                } else {
                    // set profile validation in session as valid
                    if (req.session && req.session.user && req.session.user.valid && typeof req.session.user.valid.profile !== 'undefined') {
                        req.session.user.valid.profile = true;
                    }
                    user.fields = req.body.fields;

                    user.save(function (err) {
                        if (err) {
                            res.status(500);
                            return res.send('Error occured')
                        }
                        return res.send({success:true})
                    })
                
                }
            }
        })
    });

    app.get('/user/schema', function (req, res, next) {
        res.json(userSchema);
    });

    return app;
};

// Bauhaus render method

module.exports = function (req, res, data, template, callback) {

    var viewData = data.toObject();
    data.content.token = req.query.token;

    res.render(template, data.content, callback);
};
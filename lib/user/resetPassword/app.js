// Security frontend REST API endpoints for password reset

var express = require('express');
var nodemailer = require('nodemailer');
var sha1 = require('sha1');
var User = require('bauhausjs/security/model/user');

module.exports = function (bauhausConfig) {
    var app = express();
    var $ = require('./settings')(bauhausConfig);
    var config = $.bauhausConfig.user;

    function resetMail(user, host) {

        var deferred = $.q.defer();
        // create attachments of all docs
        var attachments = [];

        var data = {
            host: host,
            user: JSON.parse(JSON.stringify(user))
        };

        var userEmail = user.username;

        var mailTemplateKey = config.mailResetPassword;

        //console.log(userEmail + " : " + JSON.stringify(data));
        $.email.sendTemplateMail(mailTemplateKey, userEmail, data, attachments).then(function () {
            deferred.resolve();
        }, function (err) {
            console.log("Error in sending a template Mail: " + err);
            deferred.reject(err);
        });

        return deferred.promise;
    }

    // Reset password for user by mail address
    app.post('/requestPasswordReset', function (req, res, next) {

        if (typeof req.body.username !== 'string') {
            res.status(400);
            return res.end('Please provide an E-Mail-Address');
        }

        var username = req.body.username.toLowerCase();

        User.findOne({
            username: username
        }, function (err, user) {
            if (err || user === null) {
                res.status(400);
                return res.end('Could not find user for given E-Mail-Address');
            }
            // user found, set reset token
            var time = Date.now().toString();
            user.resetPasswordToken = sha1(time + this.username + this.id);

            // save token
            user.save(function (err) {
                if (err) {
                    res.status(400);
                    return res.end('Failed to trigger password reset.');
                }

                var host = req.headers.host;

                resetMail(user, host).then(function (err, response) {
                    if (err) {
                        res.status(500);
                        return res.end('Failed to send reset password mail');
                    } else {
                        return res.json({
                            success: true
                        });
                    }
                }, function (err) {
                    res.status(500);
                    return res.end('Failed to send reset password mail');
                });
            });
        })
    });

    app.post('/resetPassword/:token', function (req, res, next) {
        var token = req.params.token;
        var password1 = req.body.password1;
        var password2 = req.body.password2;

        if (typeof token !== 'string') {
            res.status(400);
            return res.end('Missing token');
        }

        if (typeof password1 === 'string' && password1.length < 6) {
            res.status(400);
            return res.end('Password must be at least 6 characters long')
        }

        if (password1 !== password2) {
            res.status(400);
            return res.end('Passwords do not match');
        }

        User.findOne({
            resetPasswordToken: token
        }, function (err, user) {
            if (err || user === null) {
                res.status(400);
                return res.end('Invalid token');
            }
            // user for token was found, reset password
            user.setPassword(password1, function (err) {
                if (err) {
                    res.status(400);
                    return res.end('Could not reset password');
                }
                // set reset token to null
                user.resetPasswordToken = null;
                // save user
                user.save(function (err) {
                    if (err) {
                        res.status(400);
                        return res.end('Could not reset password');
                    }
                    // successfully saved password
                    return res.json({
                        success: true
                    });
                });
            })
        })
    });

    return app;
}
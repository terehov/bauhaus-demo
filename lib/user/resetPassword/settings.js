module.exports = function(pBauhausConfig){
    return {
        bauhausConfig:  pBauhausConfig,
        //Project:        require('../../project/model/project'),
        //User:           require('bauhausjs/security/model/user'),
        //UserContract:   require('../../usercontract/model/usercontract'),
        //Contract:       require('../../contract/model/contract'),
        //pg:             require('pg').native,
        //pg:             require('pg'),
        q:              require("q"),
        //smtp:           require('../email/smtp'),
        //EmailContent:   require('../email/model/email')
        email:          require('../../email')(pBauhausConfig)
    };
};
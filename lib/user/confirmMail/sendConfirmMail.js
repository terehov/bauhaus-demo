/**
 * Provides functionality to send mails to confirm user account
 *
 * @example
 *
 *      var confirmMail = require('lib/user/confirmMail/sendConfirmMail')(bauhausConfig);
 *      confirmMail.send(user, req.headers.host, function (err, response) {
 *          if (err === null) console.log('Mail sent');
 *      })
 */

//var nodemailer = require('nodemailer');

module.exports = function (bauhausConfig) {
    // registration successfully, send mail and send success response
    var $                   = require('./settings')(bauhausConfig);
    var config              = $.bauhausConfig.user;
    
    var sendConfirmMail = function(user, host){
    
        var deferred = $.q.defer();

        // create attachments of all docs
        var attachments = [];

        // data for mail render    
        var data = {
            user:           user,
            host:           host
        };
        
        var userEmail = user.username;

        var mailTemplateKey = config.mailConfirmMail;

        $.email.sendTemplateMail(mailTemplateKey, userEmail, data, attachments).then(function(){
            deferred.resolve();
        }, function(err){
            deferred.reject(err);
        });
    
    
        return deferred.promise;
    }
    
    return {
        send: function (user, host, callback) {
            sendConfirmMail(user, host).then(function(err, response) {
                callback(err, response);
            });
        }
    }

    
}
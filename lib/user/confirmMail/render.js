/**
 * This is the render method of the dynamic widget of confirm mail
 */

var User = require('bauhausjs/security/model/user');

module.exports = function (req, res, data, template, callback) {
    var result = {};
    result.message =  '';
    result.success = false;
    
    // no token passed
    if (typeof req.query.token === 'undefined') {
        result.message = 'Die Adresse die Sie aufgerufen haben enthält keinen Token, bitte klicken Sie erneut auf den Link in der E-Mail.'
        return res.render(template, result, callback);
    } else {
        var token = req.query.token;

        User.findOne({ "confirmMailToken": token }, function (err, user) {
            // no user found for token
            if (err || user === null) {
                
                if (req.session.user) {
                    // user is authorized already (was redirected after email confirmation), welcome
                    result.success = true;
                    return res.render(template, result, callback);
                } else {
                    // user is not authorized, an error must have happend
                    result.message = 'Ihre E-Mail-Adresse wurde bereits bestätigt oder der Token ist unbekannt.'
                    return res.render(template, result, callback);
                }
            }
            // user for token was found, reset password
            user.emailConfirmed = true;
            user.confirmMailToken = null;
            user.save(function (err) {
                req.login(user, function (err) {
                    // reload page
                    res.redirect(req.url);
                })

            });
        })  
    }

   
}
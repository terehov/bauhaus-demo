/**
 * This app exposes all backend REST API endpoints
 */

var express         = require('express'),
    securityApi     = require('bauhausjs/security/api'),
    pageApi         = require('bauhausjs/page/api'),
    contentApi      = require('bauhausjs/content/api'),
    documentApi     = require('bauhausjs/document/api'),
    tagApi          = require('bauhausjs/tag/api'),
    filesApi        = require('bauhausjs/files/api'),

    emailApi        = require('../lib/email/api'),

    hasPermission   = require('bauhausjs/security/middleware').hasPermission,

    baucis          = require('baucis');

module.exports = function (bauhausConfig) {
    var app = express();

    app.use(hasPermission(['backend:login']));

    app.use(documentApi(bauhausConfig));
    app.use(filesApi(bauhausConfig));

    // Init baucis controllers
    contentApi(bauhausConfig);
    pageApi(bauhausConfig);
    securityApi(bauhausConfig);
    tagApi(bauhausConfig);

    emailApi(bauhausConfig);

    // add baucis middleware which is created based on controller configuration
    app.use(baucis());

    return app;
}

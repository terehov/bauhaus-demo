/**
 * Frontend app of BauhausJS
 */

var express = require('express'),
    passport = require('passport'),
    renderStack = require('bauhausjs/page/middleware').renderStack,
    userApp = require('../../lib/user/app'),
    newsletterApp = require('../../lib/newsletter/app'),
    filesApp = require('bauhausjs/files/app'),

    flash = require('connect-flash'),
    install = require('./install');

    // password fallback for old md5 hashes
    var sha1 = require('sha1');
    var User = require('bauhausjs/security/model/user');
    var md5 = require("MD5");


module.exports = function (bauhausConfig) {
    var app = express();
    
    // Add frontend static files (layout images, css, js)
    var oneDay = (bauhausConfig.env !== 'development') ? 86400000 : 0; // caching: in milliseconds / 0 in debug mode
    app.use(express.static(__dirname + '/client', {
        maxage: oneDay
    }));


    // Add frontend apis
    app.use(userApp(bauhausConfig));
    app.use('/newsletter', newsletterApp(bauhausConfig));
    app.use('/files', filesApp(bauhausConfig));
    app.use(userApp(bauhausConfig));

    // add config to request
    app.use(function (req, res, next) {
        if (!req.bauhaus) req.bauhaus = {};

        req.bauhaus.config = bauhausConfig;
        next();
    });

    app.all('*', renderStack(bauhausConfig.pageTypes, bauhausConfig.contentTypes));

    app.post('/login',
        passport.authenticate('local', {
            failureRedirect: 'back',
            failureFlash: 'user.login.error'
        }),
        function (req, res, next) {
        
            // check if email was confirmed
            if (req.user && req.user.emailConfirmed !== true) {
                // not confirmed, log out again
                req.flash('error', 'user.email.notconfirmed');
                req.logout();
            }

            // send to referer page            
            res.redirect('back');
        }
    );

    app.get('/logout', function (req, res) {
        flash('info', 'Logged out');
        req.logout();
        req.session.user = null;
        res.redirect('/');
    });

    // Create root node and test user if they don't exist yet
    install();

    return app;
}
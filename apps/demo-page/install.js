/**
 * This install file is executed on server start to create a root
 * page and a default user if no data exists in the database
 */

var Page = require('bauhausjs/page/model/page');
var User = require('bauhausjs/security/model/user');
var debug = require('debug')('bauhausjs-demo:installer');
var mongoose = require('mongoose');
var fixtures = require('pow-mongoose-fixtures');
var path = require('path');

module.exports = function () {
    hasRoot(function (doesHaveRoot) {
        if (!doesHaveRoot) {
            debug("BauhausJS project missing root page, running initial setup");
            
            fixtures.load(path.resolve(__dirname, './fixtures/users.js'), mongoose);
            fixtures.load(path.resolve(__dirname, './fixtures/pages.js'), mongoose);
        }
    });

    function hasRoot (callback) {
        Page.findOne({ parentId: null, public: true }, function (err, rootPage) {
            if (err) {
                console.log(err); 
                return;
            }
            if (rootPage === null) {
                callback(false);   
            } else {
                callback(true);
            }

        })
    }
};
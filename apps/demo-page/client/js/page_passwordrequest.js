(function($){
    $(document).ready(_init);
    
    function _init(){
        
        var $form = $("form#password_form");
        $form.validate();
        
        $form.submit(function(e){
            e.preventDefault();
            
            var user = {
                username: $form.find("[name=username]").val(),
            };
            
            if($form.valid()){
                $.ajax({
                    type: "POST",
                    url: $form.attr("action"),
                    data: JSON.stringify(user),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function(pData){
                        $("#password_form").fadeOut(500, function(){
                            $("#password_success").fadeIn(500);
                        });
                        
                    }, 
                    error: function(pError){
                        $form.find(".validation-errors").show();
                    }
                });
            }
        });
        
    }
})(jQuery);
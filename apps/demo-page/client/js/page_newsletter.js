(function($){
    
    $("#newsletter_submit").on("click", function(){
        
        $.post("/newsletter/subscribe", {email: $("#newsletter_email").val()}, function(pReturn){
            $("#newsletter_error").hide();
            $("#newsletter_register").fadeOut(250, function(){
                $("#newsletter_success").fadeIn(250);
            });
        }).fail(function(pErr) {
            var error = pErr.responseText;
            if(error.indexOf("already") != -1){
                $("#newsletter_error").html("Diese E-Mail-Adresse ist bereits eingetragen.");
            } else {
                $("#newsletter_error").html("Diese E-Mail-Adresse scheint fehlerhaft zu sein.<br>Bitte erneut versuchen.");
            }
        });
    });

})(jQuery);
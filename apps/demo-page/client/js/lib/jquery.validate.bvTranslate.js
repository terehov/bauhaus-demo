jQuery.extend(jQuery.validator.messages, {
    required: "&times;",
    remote: "Please fix this field.",
    email: "Bitte eine valide E-Mail-Adresse eintragen.",
    url: "Bitte eine valide URL eintragen.",
    date: "Bitte ein valides Datum eintragen.",
    dateISO: "Bitte ein valides Datum eintragen (ISO).",
    number: "Bitte eine Zahl eintragen.",
    digits: "Bitte nur Ziffern eintragen.",
    creditcard: "Bitte eine valide Kreditkartennummer eintragen.",
    equalTo: "Die beiden Passwörter stimmen nicht überein.",
    accept: "Bitte einen Wert mit einer validen Endung eintragen.",
    maxlength: jQuery.validator.format("Bitte nicht mehr als {0} Zeichen eintragen."),
    minlength: jQuery.validator.format("Bitte mindestens {0} Zeichen eintragen."),
    rangelength: jQuery.validator.format("Bitte einen Wert zwischen {0} und {1} Zeichen eintragen."),
    range: jQuery.validator.format("Bitte einen Wert zwischen {0} und {1} eintragen."),
    max: jQuery.validator.format("Bitte einen Wert kleiner/gleich {0} eintragen."),
    min: jQuery.validator.format("Bitte einen Wert größer/gleich {0} eintragen.")
});
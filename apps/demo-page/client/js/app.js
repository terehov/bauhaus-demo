(function($){
    $(document).ready(_init);
    
    function _init(){
        
        // format numbers
        $("output.format_int").formatter("int");
        $("output.format_float").formatter("float");
        $("output.format_currency").formatter("currency");
        
        //offcanvas menu
        $('#mobile-nav-trigger').sidr({
            name: 'responsive-menu',
            source: 'nav#header-nav',
            renaming: false
        });
        jQuery.sidr('close', 'responsive-menu'); // workaround... wont't open with first click
        // close by clicking on normal content
        $(document.body).on("click", "header, .content-area", function(e) {
            $.sidr('close','responsive-menu');
        });
        
        // start video on click
        $(document.body).on("click", ".action_video", function(){
            $(this).siblings("video").show()[0].play();
        });
        
        // open register
        $(document.body).on("click", ".action_ajaxload", function(e){
            e.preventDefault();
            $('html, body').animate({scrollTop: '0px'}, 300);
            
            var $registerArea =  $(".ajaxload .content");
            if($registerArea.hasClass("loaded")){
                $(".ajaxload").slideToggle();
            } else{
                $.get(this.href, function(pData){ // load register form
                    var $content = $(pData).find("#ajaxload");
                    $registerArea.html($content).addClass("loaded");

                    $(".ajaxload").slideToggle();
                });
            }
        });
        
    }

})(jQuery);
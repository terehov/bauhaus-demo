function getHTTPObject() {
    if (window.ActiveXObject) return new ActiveXObject("Microsoft.XMLHTTP");
    else if (window.XMLHttpRequest) return new XMLHttpRequest();
    else {
        alert("Dein Browser unterstuetzt kein AJAX!");
        return null;
    }
}

var data_typ = function data_typ() {

    // NEW FOR BAUHAUSJS
    var that = this;

    this.fopCB = function (fopReq, callback) {
        if (fopReq.readyState == 4) {
            if (callback) {
                try {
                    callback(JSON.parse(fopReq.responseText));
                } catch (e) {
                    /*console.error({
                        "error": "Invalid JSON!",
                        e: e
                    });*/
                }
            }
        }
    };

    this.fop = function (data, callback) {
        var fopReq = getHTTPObject();
        if (fopReq != null) {
            fopReq.onreadystatechange = function () {
                that.fopCB(fopReq, callback);
            };
            fopReq.open("POST", "../project/owner/fs/fopout", true);
            fopReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            fopReq.send("data=" + JSON.stringify(data));
        }
    };

    this.fsOpCB = function (fopReq, callback) {
        if (fopReq.readyState == 4 && fopReq.status == 200) {
            if (callback) {
                try {
                    callback(JSON.parse(fopReq.responseText));
                } catch (e) {
                    console.error({
                        "error": "Invalid JSON!",
                        e: e
                    });
                }
            }
        }
    };

    this.fsOp = function (data, callback, dataUrl) {
        var fopReq = getHTTPObject();
        if (fopReq != null) {
            fopReq.onreadystatechange = function () {
                that.fsOpCB(fopReq, callback);
            };
            fopReq.open("POST", "../project/owner/fs/fsop/" + data.op, true);
            fopReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            var temp = '';
            if (dataUrl) {
                temp = '&file=' + encodeURIComponent(dataUrl);
            }
            fopReq.send("data=" + JSON.stringify(data) + temp);
        }
    };

    this.uploadFileById = function (id, data, callback, progress) {
        var formData = new FormData();
        formData.append("file", document.getElementById(id).files[0]);
        formData.append("data", JSON.stringify(data));

        var xhr = getHTTPObject();

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                callback(false, xhr);
            }
        };

        xhr.onerror = callback;

        if (progress && document.getElementById(progress)) {
            var progressBar = document.getElementById(progress);
            xhr.upload.onprogress = function (e) {
                if (e.lengthComputable) {
                    progressBar.value = (e.loaded / e.total) * 100;
                    progressBar.textContent = progressBar.value; // Fallback for unsupported browsers.
                }
            };
        };
        xhr.open("POST", "../project/owner/fs/upload", true);
        xhr.send(formData);
    };

};
var data = new data_typ();
/**
 * This set of directives automatically renders and validates a form by a given schema.
 *
 * Check out lib/person/templates/profile.ejs for a full example
 *
 * @example
 *      // In your controller
 *      $scope.user = {
 *           fields: {
 *               person: {
 *                   gender: 'male',
 *                   name: 'Golo Mann',
 *                   birthday: '1909-03-27'
 *               }
 *           }
 *      };
 *      // describe a schema with 3 required fields of type enum, text and date
 *      $scope.schema = {
 *           type: 'user',
 *           fields: [
 *           {
 *               name: 'fields.person.gender',
 *               type: 'enum',
 *               label: 'Anrede',
 *               options: {
 *                   enums: {
 *                       female: 'Frau',
 *                       male: 'Herr'
 *                   }
 *               }
 *           },{
 *               name: 'fields.person.name',
 *               type: 'text',
 *               label: 'Name',
 *               options: {
 *                   required: true
 *               }
 *           },{
 *               name: 'fields.person.birthday',
 *               type: 'date',
 *               label: 'Geburtsdatum',
 *               options: {
 *                   required: true
 *               }
 *           }
 *      };
 *      $scope.showErrors = false; // can be used to show errors after user clicked submit
 *
 *      <!-- Use in your template -->
 *      <div bauhaus-form ng-model="user" config="schema" show-errors="showErrors"></div>
 */
//
// @example
//


var app = angular.module('bauhaus.form', []);

app.directive('bauhausForm', function ($compile, $timeout) {
    return {
        scope: {
            doc: '=ngModel',
            config: '=config',
            showErrors: '=showErrors'
        },
        compile: function compile(el, attrs) {
            return {
                pre: function (scope, el, attr) {

                    function renderField(field, id) {
                        return '<bauhaus-' + field.type +
                            ' ng-model="doc.' + field.name +
                            '" field-config="config.fields[' + id + ']" show-errors="showErrors"></bauhaus-' + field.type + '>';
                    }

                    function recompileForm() {
                        if (scope.config && scope.config.fields) {
                            var html = '<div ng-form name="form">';

                            if (scope.config.fieldsets) {
                                // render with fieldsets
                                for (var fs in scope.config.fieldsets) {
                                    var fieldset = scope.config.fieldsets[fs];
                                    if (fieldset.id)
                                        html += '<fieldset><legend>' + fieldset.legend + '</legend>';

                                    for (var f in scope.config.fields) {
                                        if (scope.config.fields[f].fieldset === fieldset.id) {
                                            html += renderField(scope.config.fields[f], f)
                                        }
                                    }

                                    html += '</fieldset>';
                                }

                            } else {
                                // render without fieldset
                                for (var f in scope.config.fields) {
                                    html += renderField(scope.config.fields[f], f)
                                }
                            }

                            html += '</div>';

                            var form = $compile(html)(scope);
                            el.replaceWith(form);
                            $timeout(function () {
                                $compile(form)(scope)
                            }, 200)
                        }
                    }

                    scope.$watch('config', function (newVal, oldVal) {
                        if (newVal && newVal.fields) {
                            recompileForm();
                        }
                    });
                },
                post: function (scope, el, attrs) {

                }
            }
        }
    };
});

app.directive('bauhausText', function ($compile) {
    return {
        restrict: 'AEC',
        template: '<div class="page-content-field row-fluid">' +
            '     <label class="page-content-field-label span3">{{config.label}} {{config.options.required ? "*" : "" }}</label>' +
            '     <div class="span9">' +
            '       <input class="page-content-field-input input-big span12" type="text" name="{{config.name}}" ng-model="value" ng-disabled="config.permission === \'view\'"/>' +
            '       <div class="error" ng-show="validate.$error.required && showErrors">Bitte machen Sie eine Angabe.</div>' +
            '       </div>' +
            '</div>',
        scope: {
            value: '=ngModel',
            config: '=fieldConfig',
            showErrors: '=showErrors'
        },
        link: function (scope, el, attr, controller) {
            scope.$watch('config.name', function (newVal, oldVal) {
                if (newVal) {
                    scope.validate = scope.$parent.form[scope.config.name];
                }
            });
            var input = el.find('input')[0];
            if (scope.config && scope.config.options && typeof scope.config.options.required === 'boolean') {
                input.setAttribute('ng-required', true);
            }
            $compile(el.contents())(scope)
        }
    };
});

app.directive('bauhausTextarea', function ($compile) {
    return {
        restrict: 'AEC',
        template: '<div class="page-content-field row-fluid">' +
            '     <label class="page-content-field-label span3">{{config.label}} {{config.options.required ? "*" : "" }}</label>' +
            '     <div class="span9">' +
            '       <textarea name="{{config.name}}" class="span12" ng-model="value" ng-disabled="config.permission === \'view\'"/></textarea>' +
            '       <div class="error" ng-show="validate.$error.required && showErrors">Bitte machen Sie eine Angabe.</div>' +
            '     </div>' +
            '</div>',
        scope: {
            value: '=ngModel',
            config: '=fieldConfig',
            showErrors: '=showErrors'
        },
        link: function (scope, el, attr, controller) {
            scope.$watch('config.name', function (newVal, oldVal) {
                if (newVal) {
                    scope.validate = scope.$parent.form[scope.config.name];
                }
            });
            var input = el.find('input')[0];
            if (scope.config && scope.config.options && typeof scope.config.options.required === 'boolean') {
                input.setAttribute('ng-required', true);
            }
            $compile(el.contents())(scope)
        }
    };
});

app.directive('bauhausAddress', function ($compile) {
    return {
        restrict: 'AEC',
        template: '<div class="page-content-field row-fluid">' +
            '     <label class="page-content-field-label span3">{{config.label}} {{config.options.required ? "*" : "" }}</label>' +
            '     <div class="span9">' +
            '       <input class="page-content-field-input input-mid span9" type="text" name="{{config.name}}.street" ng-model="value.street" placeholder="Straße" ng-disabled="config.permission === \'view\'"/>' +
            '       <input class="page-content-field-input input-short span3" type="text" name="{{config.name}}.streetNo" ng-model="value.streetNo" placeholder="Hausnummer"  ng-disabled="config.permission === \'view\'"/>' +
            '      </div>' +
            '   </div><div class="row-fluid" style="margin-top:-10px">' +
            '     <div class="span9 offset3">' +
            '       <input class="page-content-field-input input-short span3" type="text" name="{{config.name}}.postcode" ng-model="value.postcode" placeholder="PLZ" ng-disabled="config.permission === \'view\'" />' +
            '       <input class="page-content-field-input input-mid span9" type="text" name="{{config.name}}.place" ng-model="value.place" placeholder="Ort"  ng-disabled="config.permission === \'view\'"/>' +
            '       <div class="error" ng-show="(validateStreet.$error.required || validateStreetNo.$error.required || validatePostcode.$error.required || validatePlace.$error.required ) && showErrors">Bitte geben Sie eine vollständige Adresse an.</div>' +
            '       </div>' +
            '</div>',
        scope: {
            value: '=ngModel',
            config: '=fieldConfig',
            showErrors: '=showErrors'
        },
        link: function (scope, el, attr, controller) {
            scope.$watch('config.name', function (newVal, oldVal) {
                if (newVal) {
                    scope.validateStreet = scope.$parent.form[scope.config.name + '.street'];
                    scope.validateStreetNo = scope.$parent.form[scope.config.name + '.streetNo'];
                    scope.validatePostcode = scope.$parent.form[scope.config.name + '.postcode'];
                    scope.validatePlace = scope.$parent.form[scope.config.name + '.place'];
                }
            });
            var input = el.find('input');
            if (scope.config && scope.config.options && typeof scope.config.options.required === 'boolean') {
                input.attr('ng-required', true);
            }
            $compile(el.contents())(scope)
        }
    };
});


app.directive('bauhausEnum', function ($compile) {
    return {
        restrict: 'AEC',
        template: '<div class="page-content-field row-fluid">' +
            '     <label class="page-content-field-label span3">{{config.label}} {{config.options.required ? "*" : "" }}</label>' +
            '     <div class="span9">' +
            '       <select ng-model="value" ng-options="name as label for (name, label) in options" name="{{config.name}}" ng-disabled="config.permission === \'view\'" placeholder="">' +
            '     </div>' +
            '</div>',
        scope: {
            value: '=ngModel',
            config: '=fieldConfig'
        },
        // directive structure differes due to duplicate options issue when using $compile
        // https://github.com/angular/angular.js/issues/4203
        // http://plnkr.co/edit/Vv5CpC?p=preview
        compile: function (cEl, attrs, tc) {
            return {
                pre: function (scope, el, attr) {
                    scope.$watch('config.name', function (newVal, oldVal) {
                        if (newVal) {
                            scope.validate = scope.$parent.form[scope.config.name];
                            scope.options = scope.config.options.enums || {};
                        }
                    });

                },
                post: function (scope, el, attr) {
                    var input = el.find('select')[0];
                    if (scope.config && scope.config.options && typeof scope.config.options.required === 'boolean') {
                        input.setAttribute('ng-required', true);
                    }
                    // recompilation makes no difference and causes error
                    // $compile()(scope);
                }
            }
        }
    };
});

app.directive('bauhausDate', function ($compile) {
    var lastYearWith18 = new Date().getYear()-17+1900;
    
    return {
        restrict: 'AEC',
        template: '<div class="page-content-field row-fluid">' +
            '     <label class="page-content-field-label span3">{{config.label}}  {{config.options.required ? "*" : "" }}</label>' +
            '     <div class="span9">' +
            '       <input type="hidden" ng-model="value" name="{{config.name}}" />' +
            '       <select ng-model="date.day" ng-options="n for n in range(1,31)" ng-disabled="config.permission === \'view\'"></select>' +
            '       <select ng-model="date.month" ng-options="n for n in range(1,12)" ng-disabled="config.permission === \'view\'"></select>' +
            '       <select ng-model="date.year" ng-options="n for n in range(1930,' + lastYearWith18 + ')" ng-disabled="config.permission === \'view\'"></select>' +
            '           <div class="error" ng-show="(validate.$error.required || validate.$error.date) && showErrors">Bitte geben Sie ein Datum an.</div>' +
            '   </div>' +
            '</div>',
        scope: {
            value: '=ngModel',
            config: '=fieldConfig',
            showErrors: '=showErrors'
        },
        link: function (scope, el, attr) {

            scope.date = {
                day: null,
                month: null,
                year: null
            }

            scope.validateField = function () {
                if (typeof scope.validate !== 'undefined' &&
                    typeof scope.config.options !== 'undefined' &&
                    typeof scope.config.options.required !== 'undefined' &&
                    scope.config.options.required === true) {

                    if (scope.value && new Date(scope.value) !== 'Invalid date') {
                        scope.validate.$setValidity('date', true);
                        if (scope.config.required === true) {
                            scope.validate.$setValidity('required', true);
                        }
                    } else {
                        scope.validate.$setValidity('date', false);
                        if (scope.config.required === true) {
                            scope.validate.$setValidity('required', false);
                        }
                    }
                }
            }

            // picks day, month and year from date object for select form
            scope.dateToSelect = function (date) {
                scope.date.day = date.getDate();
                scope.date.month = date.getMonth() + 1;
                scope.date.year = date.getFullYear();
            };

            // creates date object from single select fields
            scope.selectToDate = function (dateObj) {
                // check if all required fields are numbers
                if (typeof dateObj.day === 'number' && typeof dateObj.month === 'number' && typeof dateObj.year === 'number') {
                    var leadingZeroMonth = dateObj.month < 10 ? '0' : '';
                    var leadingZeroDay = dateObj.day < 10 ? '0' : '';
                    var date = dateObj.year + '-' +
                        leadingZeroMonth + dateObj.month + '-' +
                        leadingZeroDay + dateObj.day + 'T00:00:00.000Z';

                    scope.value = date;
                }
                scope.validateField();
            };

            // update model on every select update (=update in UI)
            scope.$watch('date', function (newVal, oldVal) {
                scope.selectToDate(newVal);
            }, true);

            // watch model value and update select on first model update
            scope.$watch('value', function (newVal, oldVal) {
                // ignore all scope.value updates, if scope.date has a value already
                if (typeof newVal === 'string' && scope.date.day === null) {
                    scope.dateToSelect(new Date(newVal));
                }
            });


            scope.$watch('config.name', function (newVal, oldVal) {
                if (newVal) {
                    scope.validate = scope.$parent.form[scope.config.name];
                    scope.validateField();
                }

                var input = el.find('input')[0];
                if (scope.config && scope.config.options && typeof scope.config.options.required === 'boolean') {
                    input.setAttribute('ng-required', true);
                }

                //$compile()(scope)
            });

            // Load labels of related documents
            scope.range = function (start, end) {
                var list = [];
                for (var i = start; i <= end; i++) {
                    list.push(i);
                }
                return list;
            }
        }
    };
});

app.directive('bauhausHtml', function () {
    return {
        restrict: 'AEC',
        template: '<div class="page-content-field row-fluid">' +
            '     <label class="page-content-field-label span3">{{config.label}} {{config.options.required ? "*" : "" }}</label>' +
            '     <div text-angular ta-toolbar="[ [\'p\',\'bold\',\'italics\',\'ul\',\'ol\',\'redo\',\'undo\'] ]" ng-model="value"></div>' +
            '</div>',

        scope: {
            value: '=ngModel',
            config: '=fieldConfig'
        }
    };
});

// This workaround is currently required to use dynamic form element names
// https://github.com/angular/angular.js/issues/1404
app.config(['$provide',
    function ($provide) {
        $provide.decorator('ngModelDirective', ['$delegate',
            function ($delegate) {
                var ngModel = $delegate[0],
                    controller = ngModel.controller;
                ngModel.controller = ['$scope', '$element', '$attrs', '$injector',
                    function (scope, element, attrs, $injector) {
                        var $interpolate = $injector.get('$interpolate');
                        attrs.$set('name', $interpolate(attrs.name || '')(scope));
                        $injector.invoke(controller, this, {
                            '$scope': scope,
                            '$element': element,
                            '$attrs': attrs
                        });
        }];
                return $delegate;
    }]);
        $provide.decorator('formDirective', ['$delegate',
            function ($delegate) {
                var form = $delegate[0],
                    controller = form.controller;
                form.controller = ['$scope', '$element', '$attrs', '$injector',
                    function (scope, element, attrs, $injector) {
                        var $interpolate = $injector.get('$interpolate');
                        attrs.$set('name', $interpolate(attrs.name || attrs.ngForm || '')(scope));
                        $injector.invoke(controller, this, {
                            '$scope': scope,
                            '$element': element,
                            '$attrs': attrs
                        });
        }];
                return $delegate;
    }]);
}]);

// ==============
app.directive('bauhausFile', function ($timeout) {
   return {
      restrict: 'AEC',
      template: '<div class="page-content-field" ng-blur="showSelect = false">' +
        '   <div class="row-fluid"><div class="span12"><label class="page-content-field-label">{{config.label}}</label></div></div>' +
        '   <div class="row-fluid" ng-if="!config.options.singlefile"><div class="span12"><span>Es sind maximal {{limit}} Datei/en aktivierbar.</span></div></div>' +
        '   <div class="row-fluid" ng-if="!config.options.singlefile"><div class="span12" ng-if="loading">Dateiliste wird geladen...</div></div>' +
        '     <div class="row-fluid" ng-if="config.options.singlefile"><div class="span12">' +
        '        <div ng-repeat="(key, id) in images" ng-class="getClass(key)">' +
        '           <div ng-if="isTypeimage(key) && !cropping.circle" style="cursor: pointer;"><img ng-click="OpenInNewTab(id)" ng-src="/files/{{id}}?reload={{reloadnumber}}" title="{{key}}" style="max-height: 40px; max-width: 100px;"> <span class="filename">{{key}}</span></div>' +
        '           <div ng-if="isTypeimage(key) && cropping.circle" style="cursor: pointer;"><img ng-click="OpenInNewTab(id)" ng-src="/files/{{id}}?reload={{reloadnumber}}" title="{{key}}" style="max-height: 50px; max-width: 100px; border-radius: 50%;"> <span class="filename">{{key}}</span></div>' +
        '           <div ng-if="!isTypeimage(key)" style="cursor: pointer;"><span ng-click="OpenInNewTab(id)" title="{{key}}"><i class="fa fa-download"></i> <span class="filename">{{key}}</span></span></div>' +
        '        </div> ' +
        '     </div></div>' +
        '     <div class="row-fluid" ng-if="!config.options.singlefile"><div class="span12">' +
        '       <table class="table" ng-if="!loading">' +
        '           <thead>' +
        '               <tr>' +
        '                   <th>Datei</th>' +
        '                   <th>Name</th>' +
        '                   <th>Aktiv</th>' +
        '                   <th>L&ouml;schen</th>' +
        '               </tr>' +
        '           </thead>' +
        '           <tbody>' +
        '               <tr ng-repeat="(key, id) in images" ng-class="getClass(key)">' +
        '                   <td ng-if="isTypeimage(key) && !cropping.circle" style="cursor: pointer;"><img ng-click="OpenInNewTab(id)" ng-src="/files/{{id}}?reload={{reloadnumber}}" title="{{key}}" style="max-height: 40px; max-width: 100px;"></td>' +
        '                   <td ng-if="isTypeimage(key) && cropping.circle" style="cursor: pointer;"><img ng-click="OpenInNewTab(id)" ng-src="/files/{{id}}?reload={{reloadnumber}}" title="{{key}}" style="max-height: 50px; max-width: 100px; border-radius: 50%;"></td>' +
        '                   <td ng-if="!isTypeimage(key)" style="cursor: pointer;"><span ng-click="OpenInNewTab(id)" title="{{key}}"><i class="fa fa-download"></i></span></td>' +
        '                   <td><i class="icon-pencil"></i>{{key}}</td>' +
        '                   <td ng-if="!config.options.singlefile"><input type="checkbox" style="opacity: 1;" ng-checked="isActive(id);" ng-click="toggleActive(id)" ng-disabled="isDisabled(id)"></td>' +
        '                   <td ng-if="!config.options.singlefile"><input type="button" value="L&ouml;schen" ng-click="deleteFile(key)"></td>' +
        '               </tr>' +
        '           </tbody>' +
        '       </table>' +
        '   </div></div>'+
        '   <div class="row-fluid"><div class="span12">Datei hinzuf&uuml;gen:</div></div>' + 
        '   <div class="row-fluid"><div class="span4"><input type="file" name="file" id="fileupload{{config.name}}" cropWidth="800" cropHeight="600" maxSize="false" circle="false"/></div><div class="span6"><input type="button" class="button" value="Upload" ng-click="uploadFile()"></div></div>' +
        '   <div class="row-fluid" ng-if="!config.options.singlefile"><div class="span12"> <progress min="0" max="100" value="0" id="fileuploadprogress{{config.name}}">0% complete</progress> <span> {{uploadState}} </span></div></div>' +
        '</div>',
      scope: {
           value: '=ngModel',
           config: '=fieldConfig'
      },
      link: function (scope, el, attr) {
           // Load labels of related documents
           if (typeof scope.config.options === 'undefined') {
               scope.config.options = {};
           }

           scope.limit = (scope.config.options.limit !== undefined && typeof scope.config.options.limit === 'number') ? scope.config.options.limit : 1;
           scope.filename = scope.config.options.filename || false;
           scope.loading = true;


           scope.loadId = function () {
               if(scope.$parent != null && scope.$parent.doc != null && scope.$parent.doc._id != null){
                   scope._id = scope.$parent.doc._id;
               } else {
                   console.error('could not find ID');
                   scope._id = null;
               }
           };

           scope.loadId();

           var t = scope.config.options.typeRegEx || '[.]*';
           scope.regex = new RegExp(t);
           scope.cropping = scope.config.options.cropping || {
               width: 600,
               height: 600,
               maxSize: false,
               circle: false
           };
           scope.reloadnumber = Date.now();
           scope.imageQuery = 'transform=resize&width=80&height=50';
           scope.images = {};
           scope.projectidjson = '{"projectid": "' + scope._id + '"}';

           scope.test = function (e) {
               //console.error("TOLLL");
           };
           scope.uploadState = 'Upload Status';

           scope.c = new cropImages();

           scope.c.listen('choice', function (e) {
               scope.uploadState = 'Datei wird eingelesen! Bitte warten...';
               //if (!scope.$$phase) {
               //    scope.$apply();
               //}
           });

           scope.c.listen('export', function (e) {
               scope.uploadState = 'Datei wird hochgeladen! Bitte warten...';
               if (!scope.$$phase) {
                   scope.$apply();
               }
               var blob = scope.dataURItoBlob(e.dataUrl);
               var name = e.file.name.split('.');
               name.pop();
               scope.uploadHandler(blob, name.join('_'));
           });

           scope.dataURItoBlob = function (dataURI) {
               // convert base64/URLEncoded data component to raw binary data held in a string
               var byteString;
               if (dataURI.split(',')[0].indexOf('base64') >= 0)
                   byteString = atob(dataURI.split(',')[1]);
               else
                   byteString = unescape(dataURI.split(',')[1]);

               // separate out the mime component
               var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

               // write the bytes of the string to a typed array
               var ia = new Uint8Array(byteString.length);
               for (var i = 0; i < byteString.length; i++) {
                   ia[i] = byteString.charCodeAt(i);
               }

               return new Blob([ia], {
                   type: mimeString
               });
           }

           scope.getHTTPObject = function () {
               if (window.ActiveXObject) return new ActiveXObject("Microsoft.XMLHTTP");
               else if (window.XMLHttpRequest) return new XMLHttpRequest();
               else {
                   alert("Dein Browser unterstuetzt kein AJAX!");
                   return null;
               }
           }

           scope.fsOp = function (data, callback, dataUrl) {
               scope.loadId();
               var fopReq = scope.getHTTPObject();
               if (fopReq != null) {
                   fopReq.onreadystatechange = function () {
                       if (fopReq.readyState == 4 && fopReq.status == 200) {
                           if (callback) {
                              try {
                                   callback(JSON.parse(fopReq.responseText));
                              } catch (e) {
                                   console.error({
                                       "error": "Invalid JSON!",
                                       e: e
                                   });
                              }
                           }
                       }
                   };
                   data.field = scope.config.name;
                   data.config = scope.config.configPath;
                   data._id = scope._id;
                   fopReq.open("POST", "/files/.operations/fsop/" + data.op + "?data=" + encodeURIComponent(JSON.stringify(data)), true);
                   fopReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                   var temp = '';
                   if (dataUrl) {
                       temp = '&file=' + encodeURIComponent(dataUrl);
                   }
                   fopReq.send("data=" + JSON.stringify(data) + temp);
               }
           };

           scope.uploadFileBlob = function (blob, data, callback, progress) {
               scope.loadId();
               var formData = new FormData();
               data.field = scope.config.name;
               data.config = scope.config.configPath;
               data._id = scope._id;
               formData.append("data", JSON.stringify(data));
               formData.append("file", blob, data.name);

               var xhr = scope.getHTTPObject();

               xhr.onreadystatechange = function () {
                   if (xhr.readyState == 4 && xhr.status == 200) {
                       callback(false, xhr);
                   }
               };

               xhr.onerror = callback;

               if (progress && document.getElementById(progress)) {
                   var progressBar = document.getElementById(progress);
                   xhr.upload.onprogress = function (e) {
                       if (e.lengthComputable) {
                           progressBar.value = (e.loaded / e.total) * 100;
                           progressBar.textContent = progressBar.value; // Fallback for unsupported browsers.
                       }
                   };
               };
               //console.log('data: ', data);
               xhr.open("POST", "/files/.operations/upload?data=" + encodeURIComponent(JSON.stringify(data)), true); // "+encodeURIComponent(JSON.stringify(data))+"
               xhr.send(formData);
           };

           scope.isTypeimage = function (key) {
               var a = key.split('.');
               if (['jpg', 'jpeg'].indexOf(a[a.length - 1].toLowerCase()) >= 0) {
                   return true;
               }
               return false;
           };

           scope.isActive = function (id) {
               scope.checkValue();
               if (scope.value.files.indexOf(id) >= 0) {
                   return true;
               } else {
                   return false;
               }
           };

           scope.isDisabled = function (id) {
               scope.checkValue();
               if (scope.value.files.indexOf(id) < 0 && scope.value.files.length == scope.limit) {
                   return true;
               } else {
                   return false;
               }
           };

           scope.toggleActive = function (id) {
               scope.checkValue();
               var ret = scope.value.files.indexOf(id);
               if (ret >= 0) {
                   scope.value.files.splice(ret, 1);
                   //console.log(scope.value.files);
                   return true;
               } else {
                   if (scope.value.files.length < scope.limit) {
                       scope.value.files.push(id);
                       return true;
                   } else {
                       alert("Es dürfen maximal " + scope.limit + " Dateien aktiviert werden!");
                       if (!scope.$$phase) {
                           scope.$apply();
                       }
                       return false;
                   }
               }
           };

           scope.activateAllReminder = false;

           scope.activateAllWhenNeeded = function () {
               if (scope.activateAllReminder) {
                   scope.activateAllReminder = false;
                   scope.activateAll();
               }
           };

           scope.activateAll = function () {
               if (scope.value.files.length < scope.limit) {
                   for (var i in scope.images) {
                       var id = scope.images[i];
                       if (scope.value.files.length < scope.limit && scope.value.files.indexOf(id) < 0) {
                           scope.value.files.push(id);
                       }
                   }
               }
           };

           scope.load = function () {
               scope.loadId();
               //console.log('try load', scope._id, scope.$parent.$parent);
               var k = 0;
               for (var i in scope.images) {
                   k++;
                   break;
               }
               if (k < 1) {
                   if (scope._id == null) {
                       $timeout(function () {
                           //scope.load();
                       }, 200);
                   } else {
                       scope.loadId();
                       //console.log('dir', scope.dir);
                       scope.initValue();
                       scope.loadlist();
                   }
               }
           };
           scope.highlightkey = '';

           scope.getClass = function (key) {
               if (scope.highlightkey === key) {
                   return 'highlight';
               }
               return 'unhighlight';
           }

           scope.checkValue = function () {
               if (scope.value == null || scope.value.files == null) {
                   scope.value = {};
                   scope.value.files = [];
               }
           };

           scope.removeFromlist = function (id) {
               scope.checkValue();
               var ret = scope.value.files.indexOf(id);
               if (ret >= 0) {
                   scope.value.files.splice(ret, 1);
                   //console.log(scope.value.files);
               }
           };

           scope.checkLostFiles = function () {
               scope.checkValue();
               var rem = [];
               for (var i in scope.value.files) {
                   var arr = scope.value.files[i].split('/');
                   var key = arr[arr.length - 1];
                   if (!scope.images[key]) {
                       rem.push(scope.value.files[i]);
                   }
               }
               for (var i in rem) {
                   scope.removeFromlist(rem[i]);
               }
           };

           scope.loadfailconter = 5;

           scope.loadlist = function (startCrop) {
               scope.loading = true;
               scope.fsOp({
                   "op": "readcontainersure"
               }, function (e) {
                   if (e.success) {
                       //console.log('sta');
                       for (var i in scope.images) {
                           delete scope.images[i];
                       }

                       for (var i in e.files) {
                           scope.images[e.files[i].name] = e.files[i].container.replace(/\./g, '/') + "/" + e.files[i].name;
                       }
                       scope.checkLostFiles();
                       scope.activateAllWhenNeeded();
                       //scope.images = temp;

                       scope.highlightkey = '';
                       if (startCrop) {
                           scope.highlightkey = scope.images.length - 1;
                           $timeout(function () {
                              scope.highlightkey = '';
                           }, 1500);
                       }
                       scope.reloadnumber = Date.now();

                       scope.loading = false;

                       if (!scope.$$phase) {
                           scope.$apply();
                       }
                   } else {
                       console.error("Error: " + e.info);
                       if(scope.loadfailconter > 0){
                          scope.loadfailconter--;
                          scope.loadlist();
                       }
                   }
               });
           };

           scope.deleteFile = function (id) {
               var ret = confirm("Soll diese Datei wirklich gelöscht werden?");
               if (ret) {
                   scope.fsOp({
                       "op": "removefile",
                       "file": id
                   }, function (e) {
                       if (e.success) {
                           scope.loadlist();
                       } else {
                           scope.loadlist();
                           alert("Löschen Fehlgeschlagen!");
                       }
                   });
               }
           };

           // Init relation object, of it doesn't exist yet
           scope.initValue = function () {
               if (typeof scope.value !== 'object') {
                   scope.value = {
                       files: []
                   };
               }
           };

           scope.OpenInNewTab = function (url) {
               var win = window.open('/files/' + url, '_blank');
               win.focus();
           }

           scope.uploadFile = function () {
               var uploadId = 'fileupload' + scope.config.name;
               if (scope.regex.test(document.getElementById(uploadId).files[0].type)) {
                   if (document.getElementById(uploadId).files[0].type.split('/')[0] == 'image') {
                       scope.c.crop(document.getElementById(uploadId).files[0], document.getElementById(uploadId), scope.cropping);
                   } else {
                       scope.uploadState = "Datei wird hochgeladen...";
                       var blob = document.getElementById(uploadId).files[0];
                       scope.uploadHandler(blob);
                   }
               } else {
                   scope.uploadState = "Dateityp nicht erlaubt!";
               }
           };

           scope.uploadHandler = function (blob, name) {
               var uploadProgressId = 'fileuploadprogress' + scope.config.name;
               scope.uploadFileBlob(blob, {'name': name}, function (err, data) {
                   if (err) {
                       scope.uploadState = "Upload fehlgeschlagen!";
                   } else {
                       var json = {};
                       //console.log('data.responseText',data.responseText);
                       try {
                           json = JSON.parse(data.responseText);
                       } catch (e) {
                           scope.uploadState = "Upload fehlgeschlagen!";
                       }
                       if (json.success) {
                           scope.uploadState = "Datei wurde erfolgreich hochgeladen!";
                           scope.activateAllReminder = true;
                           scope.loadlist();
                       } else {
                           scope.uploadState = "Upload fehlgeschlagen!";
                       }
                   }

                   if (!scope.$$phase) {
                       scope.$apply();
                   }
               }, uploadProgressId);
           };

           $timeout(function () {
               scope.load();
           }, 1000);

           scope.$watch('value', function (newValue, oldValue) {
               if (newValue) {
                   scope.load();
               }
           }, true);

      }
   };
});

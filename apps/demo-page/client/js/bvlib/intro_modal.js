(function ($) {
    
    // show modal
    if(getCookie("intro_modal") == "")
        _showModal();
    
    function _showModal(){
        $(".modal").addClass("active").find("div:first-child").delay(750).slideDown(1250, function(){
            _activateTrigger();
            window._countModal();
        });
    }
    
    function _activateTrigger(){
    
        $(".action_modal_close").on("click", _hideModal);
        $(".modal.active").on("click", _hideModal);
        $(document).keyup(function(e) {
            if (e.keyCode == 27) {
                _hideModal();
            }
        });
    }
    
    function _hideModal(e){
        // close only on background click
        if(e == null || e.target === $(".modal")[0] || e.target === $(".action_modal_close")[0] || e.target === $(".action_modal_close i")[0]){
            $(".modal>div").slideUp(750, function(){
                var that = this;
                
                window.setTimeout(function(){
                    $(that).parents(".modal").removeClass("active");
                    // display only once
                    setCookie("intro_modal", new Date(), 365)
                }, 750);
            });
        }
    }
    
    
    /* helper */
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }
    
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
        }
        return "";
    }
    

})(jQuery);
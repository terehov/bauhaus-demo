(function($) {
 
    $.fn.bvTabsAndAccordion = function() {
        
        this.each(function() {
            _init.call(this);
        });
        
        function _init(){
            var $this = $(this);
            
            // hide all, but first
            $this.find(".tab").removeClass("active").first().addClass("active");
            $this.find(">ul>li").removeClass("active").first().addClass("active");
            $this.find(".tabBtn").removeClass("active").first().addClass("active");
            
            // create events
            // activate tabs
            $this.find(">ul>li").click(function(e){
                e.preventDefault();
                var goal = $(this).find("a").attr("href");
                activateTab.call($this, goal, $(this));
            });
            // toggle accordion
            $this.find(".tabBtn").click(function(e){
                //e.preventDefault();
                // open
                var goal = $(this).find("a").attr("href");
                activateTab.call($this, goal, $(this));
            });
            
            // toggle tabe size
            $this.find(".toggleTabSize").click(function(e){
                e.preventDefault();       

                if($this.hasClass("open"))
                    $this.removeClass("open");
                else
                    $this.addClass("open");
            });
            
            // open on mouse over
            $this.hover(function(){
                    $this.addClass("open");
            },
            function(){
                    $this.removeClass("open");
            });
        }
        
        function activateTab(pGoal, p$Btn){
            var $this = this;

            if($this.find(pGoal).hasClass("active") && !$this.find(pGoal).hasClass("mobile_hide_tab"))
                deactivateTab.call(this, p$Btn);
            else{
                // open tab
                $this.find(".tab").removeClass("active").removeClass("mobile_hide_tab").filter(pGoal).addClass("active");
                // remove marker from tab
                $this.find(">ul>li, .tabBtn").removeClass("active");
                // active actual tab
                p$Btn.addClass("active");
            }
        }

        function deactivateTab(p$Btn){
            var $this = this;
            // close tab
            $this.find(".tab").addClass("mobile_hide_tab");
        }
       
        
        // return this for chaining
        return this;
 
    };
 
}(jQuery));
(function($){
    $(document).ready(_init);
    
    function _init(){
        
        var $form = $("form#register_form");
        
        
        // toggle company name if not private
        $form.find("[name=type]").change(function(){
            if(this.value != "person")
                $form.find("[name=company]").show();
            else
                $form.find("[name=company]").hide();
        });
        
        $form.validate();
        $form.submit(function(e){
            e.preventDefault();
            
            var user = {
                username: $form.find("[name=username]").val(),
                password: $form.find("[name=password]").val(),
                gender: $form.find("[name=gender]:checked").val(),
                firstname: $form.find("[name=firstname]").val(),
                lastname: $form.find("[name=lastname]").val(),
                type: $form.find("[name=type]").val(),
                company: $form.find("[name=company]").val(),
                newsletter: $form.find("[name=newsletter]:checked").val(),
                referral: getParameterByName("r")
            };
            
            if($form.valid()){
                $.ajax({
                    type: "POST",
                    url: $form.attr("action"),
                    data: JSON.stringify(user),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function(pData){
                        $("#register_form").fadeOut(500, function(){
                            $("#register_success").fadeIn(500);
                        });
                        
                    }, 
                    error: function(pError){
                        var errors = pError.responseJSON.error.errors;
                        $form.find("[name]").removeClass("error");
                        for(var name in errors){
                            $form.find(".validation-errors").show();
                            $form.find("[name="+name+"]").addClass("error");
                            if(errors[name].name === "BadRequestError"){
                                $form.find(".validation-errors").find("p").show();
                            }
                        }
                    }
                });
            }
        });
        
    }
})(jQuery);
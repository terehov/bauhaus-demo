var ObjectId = require('mongoose').Types.ObjectId;


exports.Role = {
    admin: {
        _id: new ObjectId(),
        name: 'Admin',
        permissions: {
            'backend:login': true
        }
    }
};


// password: test
exports.User = {
    admin: {
        username: 'admin@bauhaus.io',
        login: {
            hash: '035ffb8ce7561e99f58f14822c7ac5689f298efaecf9b70756bc63777796722fe3f6e1279b0cd8f567c735c4d6ace379d0d8ef7a515ab0ddb41046be540470bd4ba30f9262c712eed0584cb77bf4a69a3c619c3ad249b815e575db5bdf2add8dbceede3f3c4eec9bcd64efd58f914034d31233ca08d40454e840aeff51490034e8cb1a640c9ec890ac1bd64c323d22986e49503512f770747f7a0d7ffd3f1c29cb34f56e140c501938d9073fd2f77a7551ba33c72bc2c3d9ba6885037b3bf91e9efd90076f08b983450d934eb35a56c4db985fe1c71d758f8da54409a149179aa2f6a13bfae58fc0527087525d7b8a49340f7fd31b3b34c2593c6e310e201f0d6aee7920334884f64ea8151c80de19ea1c6c2e0f7a3b14401a4c5775149202dcfb08b211d3bb981e5edbec2f07e369832f8070732060a134dee2d362281d619b7c21d87a3596cae9fdc106dfb6ccc9254626e90109209f4700db1001f709f5d6dd84734119fd4758a13fcb6e1fd113ba3b5778b5d15de9edd67773d8262a996bb359724a70b4471d346e510e6cd3b223141c1e27488fc472f4d3f19c97fe40579f27b7030623cc6beb1bb9eff7616d8351b016047d0295f7d2ac8202e79e3157bac5280e55ee8a5fae320d58ae71d8b01d0d915e286207a1ec4fc3b1e49e6bc2d95d958d7fd01fdd9d0a6df5c5df88b341a61725327528828285263806a36442',
            salt: '08ab0a2ea3809faafad44051f4ec8de01333336ed8dab7e06b9b2cad94049109'
        },
        emailConfirmed: true,
        roles: [
            exports.Role.admin._id
        ]
    }
};
var ObjectId = require('mongoose').Types.ObjectId;

exports.Page = {
    home: {
        _id: new ObjectId(),
        title: 'BauhausJS-Demo',
        route: '/',
        _type: 'layout',
        public: true
    }
};
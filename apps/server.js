/**
 * This is the root server app, were all other apps (frontend, backend, api) are added to
 */

var express = require('express'),
    mongoose = require('mongoose'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    multer = require('multer'),
    flash = require('connect-flash'),
    MongoStore = require('connect-mongo')(session),
    passport = require('../config/passport'),
    securityMiddleware = require('bauhausjs/security/middleware');

module.exports = function (bauhausConfig) {
    var server = express();

    // enforce SSL
    server.use(function(req, res, next) {
      if(bauhausConfig.env !== "development" && req.headers['x-forwarded-proto']!='https') { 
        return res.redirect(['https://', req.get('Host'), req.url].join(''));
      }
      next();
    });

    // Add app middleware
    server.use(cookieParser());
    server.use(bodyParser.json());
    server.use(bodyParser.urlencoded({ extended: false })); // {limit: '100mb'} für limit
    server.use(multer()); // multipart middleware
    server.use(session({
        store: new MongoStore({ mongooseConnection: mongoose.connection }),
        secret: bauhausConfig.security.sessionSecret,
        path: '/',
        maxAge: 100 * 60 * 60 * 24,
        resave: false,
        saveUninitialized: false
    }));
    server.use(flash());
    server.use(passport.initialize());
    server.use(passport.session());
    server.use(securityMiddleware.loadUser);

    return server;
};
